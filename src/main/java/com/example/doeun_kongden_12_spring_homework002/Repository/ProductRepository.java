package com.example.doeun_kongden_12_spring_homework002.Repository;

import com.example.doeun_kongden_12_spring_homework002.Models.CustomerModel;
import com.example.doeun_kongden_12_spring_homework002.Models.ProductModel;
import com.example.doeun_kongden_12_spring_homework002.Models.Request.ProductRequest;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface ProductRepository {

    @Select("select * from product_tb")
    @Results(id = "productMap", value = {
            @Result(property = "product_id", column = "product_id"),
            @Result(property = "product_name", column = "product_name"),
            @Result(property = "product_price", column = "product_price")
    })
    List<ProductModel> getAllProduct();

    @Select("insert into product_tb (product_name, product_price) VALUES (#{productRequest.product_name},#{productRequest.product_price}) returning *")
    @ResultMap("productMap")
    ProductModel postNewProduct(@Param("productRequest") ProductRequest productRequest);

    @Select("select * from product_tb where product_id = #{id}")
    @ResultMap("productMap")
    ProductModel getProductById(Integer id);

    @Select("UPDATE product_tb SET product_name = #{productRequest.product_name} , product_price= #{productRequest.product_price}  WHERE product_id = #{id} RETURNING *")
    @ResultMap("productMap")
    ProductModel updateProductById(@Param("productRequest") ProductRequest productRequest, Integer id);

    @Select("delete FROM product_tb where product_id = #{id} returning *;")
    @ResultMap("productMap")
    ProductModel deleteProductById(Integer id);
}
