package com.example.doeun_kongden_12_spring_homework002.Repository;

import com.example.doeun_kongden_12_spring_homework002.Models.InvoiceModel;
import com.example.doeun_kongden_12_spring_homework002.Models.ProductModel;
import com.example.doeun_kongden_12_spring_homework002.Models.Request.InvoiceRequest;
import org.apache.ibatis.annotations.*;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Mapper
public interface InvoiceRepository {
    @Select("select pt.product_id,pt.product_name,pt.product_price from product_tb pt inner join invoice_detail_tb idt on pt.product_id = idt.product_id where invoice_id = #{invoice_id}")
    @Result(property = "product_id",column = "product_id")
    @Result(property = "product_name",column = "product_name")
    @Result(property = "product_price",column = "product_price")
    List<ProductModel> getAllProductByInvoiceId(Integer invoice_id);

    @Select("select * from invoice_tb")
    @Results(id = "mapInvoice", value = {
            @Result(property = "invoice_id", column = "invoice_id"),
            @Result(property = "invoice_date", column = "invoice_date"),
            //This method below will get Customer By id and return customer object and map it into
            @Result(property = "customerModel", column = "customer_id", one = @One(select = "com.example.doeun_kongden_12_spring_homework002.Repository.CustomerRepository.getCustomerById")),
            @Result(property = "productModelList", column = "invoice_id", many = @Many(select = "getAllProductByInvoiceId"))
    })
    List<InvoiceModel> getAllInvoice();


    @Select("select * from invoice_tb where invoice_id = #{invoice_id}")
    @ResultMap("mapInvoice")
    InvoiceModel getInvoiceById(Integer invoice_id);

    @Select("insert into invoice_tb(invoice_date,customer_id) values (#{invoiceRequest.invoice_date},#{invoiceRequest.customer_id}) returning invoice_id ")
    Integer addNewInvoice(@Param("invoiceRequest") InvoiceRequest invoiceRequest);

    @Select("insert into invoice_detail_tb(invoice_id,product_id) values (#{invoiceId},#{productId})")
    void addProductIdToInvoiceDetail(Integer invoiceId, Integer productId);

    @Select("update invoice_tb set customer_id = #{invoiceRequest.customer_id} where invoice_id = #{invoice_id} returning invoice_id")
    Integer updateInvoiceById(Integer invoice_id,@Param("invoiceRequest") InvoiceRequest invoiceRequest);

    @Delete("delete from invoice_detail_tb where invoice_id = #{invoice_id}")
    void deleteProductInInvoiceDetail(Integer invoice_id);

    @Select("delete from invoice_tb where invoice_id = #{invoice_id}")
    @ResultMap("mapInvoice")
    InvoiceModel deleteInvoiceById(Integer invoice_id);
}
