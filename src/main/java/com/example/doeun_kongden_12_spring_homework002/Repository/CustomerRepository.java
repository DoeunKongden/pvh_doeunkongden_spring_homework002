package com.example.doeun_kongden_12_spring_homework002.Repository;

import com.example.doeun_kongden_12_spring_homework002.Models.CustomerModel;
import com.example.doeun_kongden_12_spring_homework002.Models.Request.CustomerRequest;
import org.apache.ibatis.annotations.*;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@Mapper
public interface CustomerRepository {
    @Select("SELECT * FROM customer_tb")
    @Results(id = "customerMap", value = {
            @Result(property = "customer_id", column = "customer_id"),
            @Result(property = "customer_name", column = "customer_name"),
            @Result(property = "customer_address", column = "customer_address"),
            @Result(property = "customer_phone", column = "customer_phone")
    })
    List<CustomerModel> getAllCustomer();

    @Select("INSERT INTO customer_tb (customer_name, customer_address, customer_phone) values (#{customerRequest.customer_name}, " +
            "#{customerRequest.customer_address}, " +
            "#{customerRequest.customer_phone}) RETURNING *")
    @ResultMap("customerMap")
    CustomerModel insertNewCustomer(@Param("customerRequest") CustomerRequest customerRequest);

    @Select("SELECT * FROM customer_tb where customer_id = #{id}")
    @ResultMap("customerMap")
    CustomerModel getCustomerById(Integer id);

    @Select("UPDATE customer_tb SET customer_name = #{customerRequest.customer_name} , customer_address= #{customerRequest.customer_address} , customer_phone= #{customerRequest.customer_phone} WHERE customer_id = #{id} RETURNING *")
    @ResultMap("customerMap")
    CustomerModel updateCustomer(@Param("customerRequest") CustomerRequest customerRequest,int id);

    @Select("delete FROM customer_tb where customer_id = #{id} returning *;")
    CustomerModel deleteCustomerById(Integer id);

}
