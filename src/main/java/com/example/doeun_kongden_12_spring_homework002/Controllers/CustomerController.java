package com.example.doeun_kongden_12_spring_homework002.Controllers;

import com.example.doeun_kongden_12_spring_homework002.Models.CustomerModel;
import com.example.doeun_kongden_12_spring_homework002.Models.Request.CustomerRequest;
import com.example.doeun_kongden_12_spring_homework002.Models.Response.CustomerResponse;
import com.example.doeun_kongden_12_spring_homework002.Service.CustomerService;
import org.apache.coyote.Response;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/customer")
public class CustomerController {

    //Dependency Injection (Inject by Constructor --> injecting service into controller)
    private final CustomerService customerService;

    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @GetMapping("/get-all-customer")
    public ResponseEntity<CustomerResponse<List<CustomerModel>>> getAllCustomer() {
        CustomerResponse<List<CustomerModel>> customerResponse = CustomerResponse.<List<CustomerModel>>builder()
                .payload(customerService.getAllCustomer())
                .successStatus(true)
                .message("Successfully Fetch All Customer").build();
        return ResponseEntity.ok().body(customerResponse);
    }

    @PostMapping("/post-new-customer")
    public ResponseEntity<CustomerResponse<CustomerModel>> insertNewCustomer(@RequestBody CustomerRequest customerRequest) {
        CustomerResponse<CustomerModel> customerResponse = CustomerResponse.<CustomerModel>builder()
                .payload(customerService.postNewCustomer(customerRequest))
                .successStatus(true)
                .message("Post New Customer Successful").build();
        return ResponseEntity.ok().body(customerResponse);
    }

    @GetMapping("/get-customer-by-id/{id}")
    public ResponseEntity<CustomerResponse<CustomerModel>> getCustomerById(@PathVariable("id") Integer id) {
        CustomerResponse<CustomerModel> customerResponse = CustomerResponse.<CustomerModel>builder()
                .payload(customerService.getCustomerById(id))
                .successStatus(true)
                .message("Fetch Customer Succesful").build();

        return ResponseEntity.ok().body(customerResponse);
    }

    @PutMapping("/update-customer-by-id/{id}")
    public ResponseEntity<CustomerResponse<CustomerModel>> updateCustomerById(@RequestBody CustomerRequest customerRequest, @PathVariable("id") Integer id) {
        CustomerResponse<CustomerModel> customerResponse = CustomerResponse.<CustomerModel>builder()
                .payload(customerService.updateCustomer(customerRequest, id))
                .successStatus(true)
                .message("Update Customer successful")
                .build();
        return ResponseEntity.ok().body(customerResponse);
    }

    @DeleteMapping("/delete-customer-by-id/{id}")
    public ResponseEntity<CustomerResponse<CustomerModel>> deleteCustomerById(@PathVariable("id") Integer id){
        CustomerResponse<CustomerModel> customerResponse = CustomerResponse.<CustomerModel>builder()
                .payload(customerService.deleteCustomerById(id))
                .message("Delete Customer Successful")
                .successStatus(true)
                .build();
        return ResponseEntity.ok().body(customerResponse);
    }
}
