package com.example.doeun_kongden_12_spring_homework002.Controllers;

import com.example.doeun_kongden_12_spring_homework002.Models.InvoiceModel;
import com.example.doeun_kongden_12_spring_homework002.Models.Request.InvoiceRequest;
import com.example.doeun_kongden_12_spring_homework002.Models.Response.InvoiceResponse;
import com.example.doeun_kongden_12_spring_homework002.Service.InvoiceService;
import org.apache.ibatis.annotations.Delete;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/invoice")
public class InvoiceController {

    private final InvoiceService invoiceService;

    public InvoiceController(InvoiceService invoiceService) {
        this.invoiceService = invoiceService;
    }


    @GetMapping("/get-all-invoice")
    public ResponseEntity<InvoiceResponse<List<InvoiceModel>>> getAllInvoice() {

        InvoiceResponse<List<InvoiceModel>> invoiceResponse = InvoiceResponse.<List<InvoiceModel>>builder()
                .message("Fetch Invoice Successfully")
                .payload(invoiceService.getAllInvoice())
                .successStatus(true)
                .build();
        return ResponseEntity.ok().body(invoiceResponse);
    }

    @GetMapping("/get-invoice-by-id/{id}")
    public ResponseEntity<InvoiceResponse<InvoiceModel>> getInvoiceById(@RequestParam("id") Integer id) {
        InvoiceResponse<InvoiceModel> invoiceResponse = InvoiceResponse.<InvoiceModel>builder()
                .message("Fetch Invoice Successfully")
                .payload(invoiceService.getInvoiceById(id))
                .successStatus(true)
                .build();
        return ResponseEntity.ok().body(invoiceResponse);
    }

    @PostMapping("/add-new-invoice")
    public ResponseEntity<InvoiceResponse<InvoiceModel>> addNewInvoice(
            @RequestBody InvoiceRequest invoiceRequest
    ) {

        int invoice_id = invoiceService.addNewInvoice(invoiceRequest);

        InvoiceModel invoiceModel =  invoiceService.getInvoiceById(invoice_id);

        InvoiceResponse<InvoiceModel> invoiceResponse = InvoiceResponse.<InvoiceModel>builder()
                .message("Add New Invoice Successful")
                .payload(invoiceModel)
                .successStatus(true)
                .build();

        return ResponseEntity.ok().body(invoiceResponse);
    }

    @PutMapping("/update-invoice-by-id/{id}")
    public ResponseEntity<InvoiceResponse<InvoiceModel>> updateInvoiceById(@PathVariable("id")Integer id,@RequestBody InvoiceRequest invoiceRequest){
        Integer invoice_id = invoiceService.updateInvoiceById(id,invoiceRequest);
        InvoiceModel invoiceModel = invoiceService.getInvoiceById(invoice_id);
        InvoiceResponse<InvoiceModel> invoiceResponse = InvoiceResponse.<InvoiceModel>builder()
                .message("Update Invoice Successful")
                .payload(invoiceModel)
                .successStatus(true)
                .build();

        return ResponseEntity.ok().body(invoiceResponse);
    }

    @DeleteMapping("/delete-invoice-by-id/{id}")
    public ResponseEntity<InvoiceResponse<InvoiceModel>> deleteInvoiceById(@RequestParam("id") Integer id){
          InvoiceResponse<InvoiceModel> invoiceResponse = InvoiceResponse.<InvoiceModel>builder()
                  .message("Delete Invoice Successfully")
                  .payload(invoiceService.deleteInvoiceById(id))
                  .successStatus(true)
                  .build();
          return ResponseEntity.ok().body(invoiceResponse);
    }
}
