package com.example.doeun_kongden_12_spring_homework002.Controllers;


import com.example.doeun_kongden_12_spring_homework002.Models.CustomerModel;
import com.example.doeun_kongden_12_spring_homework002.Models.ProductModel;
import com.example.doeun_kongden_12_spring_homework002.Models.Request.ProductRequest;
import com.example.doeun_kongden_12_spring_homework002.Models.Response.CustomerResponse;
import com.example.doeun_kongden_12_spring_homework002.Models.Response.ProductResponse;
import com.example.doeun_kongden_12_spring_homework002.Service.ProductService;
import org.apache.coyote.Response;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1/product")
public class ProductController {

    //Dependencies injection
    public final ProductService productService;
    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @GetMapping("/get-all-product")
    public ResponseEntity<ProductResponse<List<ProductModel>>> getAllProduct() {
        ProductResponse<List<ProductModel>> productResponse = ProductResponse.<List<ProductModel>>builder()
                .payload(productService.getAllProduct())
                .successStatus(true)
                .message("Successfully Fetch All Customer").build();
        return ResponseEntity.ok().body(productResponse);
    }

    @PostMapping("/post-new-product")
    public ResponseEntity<ProductResponse<ProductModel>> postNewProduct(@RequestBody ProductRequest productRequest){
        ProductResponse<ProductModel> productResponse = ProductResponse.<ProductModel>builder()
                .payload(productService.postNewProduct(productRequest))
                .successStatus(true)
                .message("Successfully Post New Product")
                .build();
        return ResponseEntity.ok().body(productResponse);
    }

    @GetMapping("/get-product-by-id/{id}")
    public ResponseEntity<ProductResponse<ProductModel>> getProductById(@RequestParam("id")Integer id){
        ProductResponse<ProductModel> productResponse = ProductResponse.<ProductModel>builder()
                .payload(productService.getProdcutById(id))
                .successStatus(true)
                .message("Successfully Fetch Product")
                .build();
        return ResponseEntity.ok().body(productResponse);
    }

    @PutMapping("/update-product/{id}")
    public ResponseEntity<ProductResponse<ProductModel>> updateProductById(@RequestBody ProductRequest productRequest,@RequestParam("id")Integer id){
        ProductResponse<ProductModel> productResponse = ProductResponse.<ProductModel>builder()
                .payload(productService.updateProductById(productRequest,id))
                .message("Update Product Successful")
                .successStatus(true)
                .build();
        return ResponseEntity.ok().body(productResponse);
    }

    @DeleteMapping("/delete-product-id/{id}")
    public ResponseEntity<ProductResponse<ProductModel>> deleteProductById(@RequestParam("id")Integer id){
        ProductResponse<ProductModel> productResponse = ProductResponse.<ProductModel>builder()
                .payload(productService.deleteProductById(id))
                .successStatus(true)
                .message("Delete Product Successful")
                .build();
        return ResponseEntity.ok().body(productResponse);
    }
}
