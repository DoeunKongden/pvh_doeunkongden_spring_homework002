package com.example.doeun_kongden_12_spring_homework002;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DoeunKongden12SpringHomework002Application {

    public static void main(String[] args) {
        SpringApplication.run(DoeunKongden12SpringHomework002Application.class, args);
    }

}
