create table product_tb(
    product_id serial primary key,
    product_name varchar(200) not null,
    product_price float not null
);

create table invoice_detail_tb(
    id serial primary key,
    invoice_id int,
    product_id int
);
create table invoice_tb(
    invoice_id serial primary key,
    invoice_date date,
    customer_id int,
    constraint fk_customer foreign key (customer_id) references customer_tb(customer_id) on delete cascade on update cascade
);

create table customer_tb
(
    customer_id      serial primary key,
    customer_name    varchar(200) not null,
    customer_address varchar(200) not null,
    customer_phone   int          not null
);

create table invoice_detail_tb(
    id serial primary key ,
    invoice_id int,
    constraint fk_invoice_id foreign key (invoice_id) references invoice_tb(invoice_id) on delete  cascade on update cascade,
    product_id int,
    constraint fk_product_id foreign key (product_id) references product_tb(product_id) on delete cascade on update cascade
);
