package com.example.doeun_kongden_12_spring_homework002.Models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProductModel {
    private Integer product_id;
    private String product_name;
    private double product_price;
}
