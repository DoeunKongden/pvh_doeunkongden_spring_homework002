package com.example.doeun_kongden_12_spring_homework002.Models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CustomerModel {
    private Integer customer_id;
    private String customer_name;
    private String customer_phone;
    private String customer_address;

}
