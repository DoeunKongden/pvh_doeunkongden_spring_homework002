package com.example.doeun_kongden_12_spring_homework002.Models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.cglib.core.Local;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class InvoiceModel {
    private Integer invoice_id;
    private LocalDateTime invoice_date;
    private CustomerModel customerModel;
    private List<ProductModel> productModelList;
}
