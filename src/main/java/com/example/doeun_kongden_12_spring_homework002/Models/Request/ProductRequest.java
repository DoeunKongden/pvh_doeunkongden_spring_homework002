package com.example.doeun_kongden_12_spring_homework002.Models.Request;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductRequest {
    private String product_name;
    private double product_price;
}
