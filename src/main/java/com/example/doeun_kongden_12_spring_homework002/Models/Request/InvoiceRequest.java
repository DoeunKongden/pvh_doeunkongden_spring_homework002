package com.example.doeun_kongden_12_spring_homework002.Models.Request;


import com.example.doeun_kongden_12_spring_homework002.Models.ProductModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class InvoiceRequest {
    private Integer  customer_id;
    private LocalDateTime invoice_date;
    private List<Integer> product_id;

}
