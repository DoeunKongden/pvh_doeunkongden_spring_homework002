package com.example.doeun_kongden_12_spring_homework002.Models.Request;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CustomerRequest {
    private String customer_name;
    private String customer_address;
    private String customer_phone;

}
