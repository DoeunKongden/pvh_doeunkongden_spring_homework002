package com.example.doeun_kongden_12_spring_homework002.Models.Response;


import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class InvoiceResponse<T> {
    private T payload;
    private String message;
    private boolean successStatus;
}
