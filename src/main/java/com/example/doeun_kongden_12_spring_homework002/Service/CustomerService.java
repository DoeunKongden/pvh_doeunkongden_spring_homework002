package com.example.doeun_kongden_12_spring_homework002.Service;

import com.example.doeun_kongden_12_spring_homework002.Models.CustomerModel;
import com.example.doeun_kongden_12_spring_homework002.Models.Request.CustomerRequest;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface CustomerService {
    List<CustomerModel> getAllCustomer();
    CustomerModel postNewCustomer(CustomerRequest customerRequest);
    CustomerModel getCustomerById(Integer id);
    CustomerModel updateCustomer(CustomerRequest customerRequest,int id);
    CustomerModel deleteCustomerById(Integer id);

}
