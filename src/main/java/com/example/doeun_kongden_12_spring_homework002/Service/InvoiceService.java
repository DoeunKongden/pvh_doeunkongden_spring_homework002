package com.example.doeun_kongden_12_spring_homework002.Service;

import com.example.doeun_kongden_12_spring_homework002.Models.InvoiceModel;
import com.example.doeun_kongden_12_spring_homework002.Models.Request.InvoiceRequest;

import java.util.List;

public interface InvoiceService {
    List<InvoiceModel> getAllInvoice();
    Integer addNewInvoice(InvoiceRequest invoiceRequest);
    InvoiceModel getInvoiceById(Integer invoice_id);

    Integer updateInvoiceById(Integer invoice_id,InvoiceRequest invoiceRequest);

    InvoiceModel deleteInvoiceById(Integer invoice_id);

}

