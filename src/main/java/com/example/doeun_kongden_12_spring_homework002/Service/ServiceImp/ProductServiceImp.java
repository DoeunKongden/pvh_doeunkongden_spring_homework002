package com.example.doeun_kongden_12_spring_homework002.Service.ServiceImp;

import com.example.doeun_kongden_12_spring_homework002.Models.ProductModel;
import com.example.doeun_kongden_12_spring_homework002.Models.Request.ProductRequest;
import com.example.doeun_kongden_12_spring_homework002.Repository.ProductRepository;
import com.example.doeun_kongden_12_spring_homework002.Service.ProductService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductServiceImp implements ProductService {

    //Dependecies injection in from the ProductRepository
    public final ProductRepository productRepository;

    public ProductServiceImp(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public List<ProductModel> getAllProduct() {
        return productRepository.getAllProduct();
    }

    @Override
    public ProductModel postNewProduct(ProductRequest productRequest) {
        return productRepository.postNewProduct(productRequest);
    }

    @Override
    public ProductModel getProdcutById(Integer id) {
        return productRepository.getProductById(id);
    }

    @Override
    public ProductModel updateProductById(ProductRequest productRequest, int id) {
        return productRepository.updateProductById(productRequest,id);
    }

    @Override
    public ProductModel deleteProductById(Integer id) {
        return productRepository.deleteProductById(id);
    }

}
