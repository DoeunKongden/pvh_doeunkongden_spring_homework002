package com.example.doeun_kongden_12_spring_homework002.Service.ServiceImp;

import com.example.doeun_kongden_12_spring_homework002.Models.CustomerModel;
import com.example.doeun_kongden_12_spring_homework002.Models.Request.CustomerRequest;
import com.example.doeun_kongden_12_spring_homework002.Repository.CustomerRepository;
import com.example.doeun_kongden_12_spring_homework002.Service.CustomerService;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class CustomerServiceImp implements CustomerService {

    //Dependency Injection (Inject By Constructor)
    public final CustomerRepository customerRepository;

    //Injecting CustomerRepository into CustomerService
    public CustomerServiceImp(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @Override
    public List<CustomerModel> getAllCustomer() {
        return customerRepository.getAllCustomer();
    }

    @Override
    public CustomerModel postNewCustomer(CustomerRequest customerRequest) {
        return customerRepository.insertNewCustomer(customerRequest);
    }

    @Override
    public CustomerModel getCustomerById(Integer id) {
        return customerRepository.getCustomerById(id);
    }

    @Override
    public CustomerModel updateCustomer(CustomerRequest customerRequest, int id) {
        return customerRepository.updateCustomer(customerRequest,id);
    }

    @Override
    public CustomerModel deleteCustomerById(Integer id) {
        return customerRepository.deleteCustomerById(id);
    }
}
