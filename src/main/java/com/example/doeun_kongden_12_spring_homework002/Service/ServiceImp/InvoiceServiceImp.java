package com.example.doeun_kongden_12_spring_homework002.Service.ServiceImp;

import com.example.doeun_kongden_12_spring_homework002.Models.InvoiceModel;
import com.example.doeun_kongden_12_spring_homework002.Models.Request.InvoiceRequest;
import com.example.doeun_kongden_12_spring_homework002.Repository.InvoiceRepository;
import com.example.doeun_kongden_12_spring_homework002.Service.InvoiceService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class InvoiceServiceImp implements InvoiceService {

    private final InvoiceRepository invoiceRepository;

    public InvoiceServiceImp(InvoiceRepository invoiceRepository) {
        this.invoiceRepository = invoiceRepository;
    }

    @Override
    public List<InvoiceModel> getAllInvoice() {
        return invoiceRepository.getAllInvoice();
    }

    @Override
    public Integer addNewInvoice(InvoiceRequest invoiceRequest) {
        Integer invoiceId = invoiceRepository.addNewInvoice(invoiceRequest);

        for (int i = 0; i < invoiceRequest.getProduct_id().size(); i++) {
            invoiceRepository.addProductIdToInvoiceDetail(invoiceId,invoiceRequest.getProduct_id().get(i));
        }
        return invoiceId;
    }

    @Override
    public InvoiceModel getInvoiceById(Integer invoice_id) {
        return invoiceRepository.getInvoiceById(invoice_id);
    }

    @Override
    public Integer updateInvoiceById(Integer invoice_id,InvoiceRequest invoiceRequest) {
        Integer invoiceById = invoiceRepository.updateInvoiceById(invoice_id,invoiceRequest);
        invoiceRepository.deleteProductInInvoiceDetail(invoiceById);
        for(int i=0; i<invoiceRequest.getProduct_id().size();i++){
            invoiceRepository.addProductIdToInvoiceDetail(invoice_id,invoiceRequest.getProduct_id().get(i));
        }
        return invoice_id;
    }

    @Override
    public InvoiceModel deleteInvoiceById(Integer invoice_id) {
        return invoiceRepository.deleteInvoiceById(invoice_id);
    }
}
