package com.example.doeun_kongden_12_spring_homework002.Service;

import com.example.doeun_kongden_12_spring_homework002.Models.ProductModel;
import com.example.doeun_kongden_12_spring_homework002.Models.Request.ProductRequest;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface ProductService {
    List<ProductModel> getAllProduct();
    ProductModel postNewProduct(ProductRequest productRequest);
    ProductModel getProdcutById(Integer id);
    ProductModel updateProductById(ProductRequest productRequest,int id);
    ProductModel deleteProductById(Integer id);
}
